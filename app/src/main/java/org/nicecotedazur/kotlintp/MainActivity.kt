package org.nicecotedazur.kotlintp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    val andVersionArray = Array<AndVersionName>(14, { AndVersionName("", 0, "") })

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        seedItems()
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = AndVersionAdapter(andVersionArray)
    }

    fun seedItems() {
        val nameArray = resources.getStringArray(R.array.andVersionName)
        val imgArray = arrayOf(R.drawable.aphrodite, R.drawable.apollon, R.drawable.ares, R.drawable.artemis, R.drawable.athena, R.drawable.demeter, R.drawable.dionysos, R.drawable.hades, R.drawable.hephaistos, R.drawable.hera, R.drawable.hermes, R.drawable.hestia ,R.drawable.poseidon, R.drawable.zeus)
        val typeArray = resources.getStringArray(R.array.andVersionType)
        for(i in 0..(nameArray.size -1)) {
            andVersionArray[i] = AndVersionName(nameArray[i], imgArray[i], typeArray[i])
        }
    }
}